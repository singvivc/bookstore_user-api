package user

import (
	"fmt"
	"gitlab.com/singvivc/bookstore_user-api/datasource"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"gitlab.com/singvivc/bookstore_utils-go/logger"
)

const (
	insertUserQuery          = "INSERT INTO users(firstName, lastName, email, password, status, createdOn, updatedOn) VALUES (?, ?, ?, ?, ?, ?, ?);"
	selectUserById           = "SELECT id, firstName, lastName, email, status, createdOn, updatedOn FROM users WHERE id = ?;"
	userUpdateQuery          = "UPDATE users SET firstName=?, lastName=?, email=?, updatedOn=?, status=? WHERE id=?;"
	deleteUserQuery          = "DELETE FROM users WHERE id=?;"
	findUserByStatusQuery    = "SELECT id, firstName, lastName, email, status, createdOn, updatedOn FROM users WHERE status = ?;"
	findByEmailPasswordQuery = "SELECT id, firstName, lastName, email, status, createdOn, updatedOn, password FROM users WHERE email = ? AND status=?;"
)

var (
	dataSource = datasource.Client
)

// GetUserById function gets the user from the database by it's id.
// if not found nil will be returned
func (user *User) Get() errors.RestError {
	statement, err := dataSource.Prepare(selectUserById)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return errors.InternalServerError("database error")
	}
	defer statement.Close()
	result := statement.QueryRow(user.Id)
	if err := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.Status, &user.CreatedOn,
		&user.UpdatedOn); err != nil {
		logger.Error("Error occurred while extracting the record", err)
		return errors.InternalServerError("database error")
	}
	return nil
}

// Save method saves the new user to the database
func (user *User) Save() errors.RestError {
	statement, err := dataSource.Prepare(insertUserQuery)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return errors.InternalServerError("database error")
	}
	defer statement.Close()
	result, err := statement.Exec(user.FirstName, user.LastName, user.Email, user.Password, user.Status,
		user.CreatedOn, user.UpdatedOn)
	if err != nil {
		logger.Error("Error occurred while saving the user", err)
		return errors.InternalServerError("database error")
	}
	userId, err := result.LastInsertId()
	if err != nil {
		logger.Error("Error occurred while fetching the id of the created user", err)
		return errors.InternalServerError("database error")
	}
	user.Id = userId
	return nil
}

// UpdateUser updates this user in the database
func (user *User) Update() errors.RestError {
	statement, err := dataSource.Prepare(userUpdateQuery)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return errors.InternalServerError("database error")
	}
	defer statement.Close()
	_, err = statement.Exec(user.FirstName, user.LastName, user.Email, user.Status, user.UpdatedOn, user.Id)
	if err != nil {
		logger.Error("Error occurred while updating the user", err)
		return errors.InternalServerError("database error")
	}
	return nil
}

func (user *User) Delete() errors.RestError {
	statement, err := dataSource.Prepare(deleteUserQuery)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return errors.InternalServerError("database error")
	}
	defer statement.Close()
	if _, err = statement.Exec(user.Id); err != nil {
		logger.Error("Error occurred while deleting the user", err)
		return errors.InternalServerError("database error")
	}
	return nil
}

func (user *User) FindByStatus(status string) ([]User, errors.RestError) {
	statement, err := dataSource.Prepare(findUserByStatusQuery)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return nil, errors.InternalServerError("database error")
	}
	defer statement.Close()

	rows, err := statement.Query(status)
	if err != nil {
		logger.Error("Error occurred while finding the user", err)
		return nil, errors.InternalServerError("database error")
	}
	defer rows.Close()
	results := make([]User, 0)
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.Status, &user.CreatedOn,
			&user.UpdatedOn); err != nil {
			logger.Error("Error occurred while fetching the user record", err)
			return nil, errors.InternalServerError("database error")
		}
		results = append(results, user)
	}
	if len(results) == 0 {
		return nil, errors.NotFoundError(fmt.Sprintf("No user found matching with '%s' status", status))
	}
	return results, nil
}

func (user *User) FindByEmailAndPassword(password string) errors.RestError {
	statement, err := dataSource.Prepare(findByEmailPasswordQuery)
	if err != nil {
		logger.Error("Error occurred while preparing the statement", err)
		return errors.InternalServerError("database error")
	}
	defer statement.Close()
	result := statement.QueryRow(user.Email, Active)
	if err := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.Status, &user.CreatedOn,
		&user.UpdatedOn, &user.Password); err != nil {
		logger.Error("Error occurred while extracting the record", err)
		return errors.InternalServerError("database error")
	}
	if err := user.ComparePassword(password); err != nil {
		logger.Error("invalid user credential. Either of user name or password is not matching", err)
		return errors.NotFoundError("invalid user credential. Either of user name or password is not matching")
	}
	return nil
}
