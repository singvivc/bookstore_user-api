package user

import "encoding/json"

type publicUser struct {
	Id        int64  `json:"id"`
	CreatedOn string `json:"createdOn"`
	UpdatedOn string `json:"updatedOn"`
	Status    string `json:"status"`
}

type internalUser struct {
	Id        int64  `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	CreatedOn string `json:"createdOn"`
	UpdatedOn string `json:"updatedOn"`
	Status    string `json:"status"`
}

func (users Users) Marshal(isPublic bool) []interface{} {
	results := make([]interface{}, len(users))
	for index, user := range users {
		results[index] = user.Marshal(isPublic)
	}
	return results
}

func (user *User) Marshal(isPublic bool) interface{} {
	userJson, _ := json.Marshal(user)
	if isPublic {
		var publicUser publicUser
		json.Unmarshal(userJson, &publicUser)
		return publicUser
	}
	var internalUser internalUser
	json.Unmarshal(userJson, &internalUser)
	return internalUser
}
