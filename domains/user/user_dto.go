package user

import (
	errors2 "errors"
	"gitlab.com/singvivc/bookstore_utils-go/date"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strings"
)

const (
	Active  = "active"
	Pending = "pending"
)

type Users []User

type User struct {
	Id        int64  `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	CreatedOn string `json:"createdOn"`
	UpdatedOn string `json:"updatedOn"`
	Status    string `json:"status"`
	Password  string `json:"password"`
}

func (user *User) Validate() errors.RestError {
	user.FirstName = strings.TrimSpace(user.FirstName)
	user.LastName = strings.TrimSpace(user.LastName)

	user.Email = strings.TrimSpace(strings.ToLower(user.Email))
	if user.Email == "" {
		return errors.BadRequestError("Invalid email address")
	}
	user.Password = strings.TrimSpace(user.Password)
	if user.Password == "" {
		return errors.BadRequestError("Invalid password")
	}
	user.Password = hashAndSalt(user.Password)
	return nil
}

func (user *User) From(from User) {
	user.FirstName = from.FirstName
	user.LastName = from.LastName
	if len(from.Email) > 0 {
		user.Email = from.Email
	}
	user.UpdatedOn = date.GetCurrentTime()
}

func hashAndSalt(password string) string {
	passwordInByte := []byte(password)
	hash, err := bcrypt.GenerateFromPassword(passwordInByte, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}

func (user *User) ComparePassword(plainPassword string) error {
	byteHashedPassword := []byte(user.Password)
	bytePlainPassword := []byte(plainPassword)

	if err := bcrypt.CompareHashAndPassword(byteHashedPassword, bytePlainPassword); err != nil {
		return errors2.New("invalid user credential. Either of user name or password is not matching")
	}
	return nil
}
