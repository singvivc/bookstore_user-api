package errors

import (
	"github.com/go-sql-driver/mysql"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"strings"
)

const (
	errorNoRows = "no rows in result set"
)

func ParseError(err error) *errors.RestError {
	if sqlError, ok := err.(*mysql.MySQLError); ok {
		return parseDatabaseError(sqlError)
	}
	if strings.Contains(err.Error(), errorNoRows) {
		return errors.NotFoundError("No record found matching for the given id")
	}
	return errors.InternalServerError("error parsing database response")
}

func parseDatabaseError(sqlErr *mysql.MySQLError) *errors.RestError {
	switch sqlErr.Number {
	case 1062:
		return errors.BadRequestError("Invalid data")
	}
	return errors.InternalServerError("error processing request")
}
