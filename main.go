package main

import (
	"gitlab.com/singvivc/bookstore_user-api/app"
)

func main() {
	app.StartBookStoreUserApplication()
}
