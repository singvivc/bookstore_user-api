package health

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetHealth(context *gin.Context) {
	context.String(http.StatusOK, "Ok")
}
