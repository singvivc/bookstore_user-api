/**
	Package user holds the function that deals with the incoming request
**/
package user

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/singvivc/bookstore_oauth-go/oauth"
	"gitlab.com/singvivc/bookstore_user-api/domains/user"
	"gitlab.com/singvivc/bookstore_user-api/services"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"net/http"
	"strconv"
)

var userService = services.UserService

// Create function creates the user from the request object
func Create(context *gin.Context) {
	var newUser user.User
	if err := context.ShouldBindJSON(&newUser); err != nil {
		restErr := errors.BadRequestError("Invalid json request")
		context.JSON(restErr.Status(), restErr)
		return
	}
	savedUser, err := userService.CreateUser(newUser)
	if err != nil {
		context.JSON(err.Status(), err)
		return
	}
	context.JSON(http.StatusCreated, savedUser.Marshal(context.GetHeader("X-Public") == "true"))
}

// Get function get the user based on the user id
func Get(context *gin.Context) {
	if err := oauth.Authenticate(context.Request); err != nil {
		context.JSON(err.Status(), err)
		return
	}
	userId, err := getUserId(context.Param("user_id"))
	if err != nil {
		context.JSON(err.Status(), err)
		return
	}
	user, restErr := userService.GetUser(userId)
	if restErr != nil {
		context.JSON(restErr.Status(), restErr)
		return
	}
	if oauth.GetCallerId(context.Request) == user.Id {
		context.JSON(http.StatusOK, user.Marshal(false))
		return
	}
	context.JSON(http.StatusOK, user.Marshal(oauth.IsPublic(context.Request)))
}

// Search function search for the given the user
func Search(context *gin.Context) {
	status := context.Query("status")
	users, err := userService.Search(status)
	if err != nil {
		context.JSON(err.Status(), err)
		return
	}
	context.JSON(http.StatusOK, users.Marshal(context.GetHeader("X-Public") == "true"))
}

func Update(context *gin.Context) {
	userId, err := getUserId(context.Param("user_id"))
	if err != nil {
		context.JSON(err.Status(), err)
		return
	}
	var user user.User
	if err := context.ShouldBindJSON(&user); err != nil {
		restErr := errors.BadRequestError(fmt.Sprintf("invalid json body"))
		context.JSON(restErr.Status(), restErr)
		return
	}
	user.Id = userId
	updatedUser, restErr := userService.UpdateUser(user)
	if restErr != nil {
		context.JSON(restErr.Status(), err)
		return
	}
	context.JSON(http.StatusOK, updatedUser.Marshal(context.GetHeader("X-Public") == "true"))
}

func Delete(context *gin.Context) {
	userId, err := getUserId(context.Param("user_id"))
	if err != nil {
		context.JSON(err.Status(), err)
		return
	}
	if err := userService.DeleteUser(userId); err != nil {
		context.JSON(err.Status(), err)
	}
	context.JSON(http.StatusOK, map[string]string{"status": "deleted"})
}

func Login(context *gin.Context) {
	var request user.LoginRequest
	if err := context.ShouldBindJSON(&request); err != nil {
		restErr := errors.BadRequestError(fmt.Sprintf("invalid json body"))
		context.JSON(restErr.Status(), restErr)
		return
	}
	user, restError := userService.LoginUser(request)
	if restError != nil {
		context.JSON(restError.Status(), restError)
		return
	}
	context.JSON(http.StatusOK, user.Marshal(context.GetHeader("X-Public") == "true"))
}

func getUserId(userIdParam string) (int64, errors.RestError) {
	userId, err := strconv.ParseInt(userIdParam, 10, 64)
	if err != nil {
		return 0, errors.BadRequestError(
			fmt.Sprintf("Invalid user id. User id '%s' should be a number", userIdParam))
	}
	return userId, nil
}
