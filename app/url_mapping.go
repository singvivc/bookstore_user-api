package app

import (
	"gitlab.com/singvivc/bookstore_user-api/controllers/health"
	"gitlab.com/singvivc/bookstore_user-api/controllers/user"
)

func mapUrls() {
	router.GET("/ping", health.GetHealth)

	router.GET("/user/:user_id", user.Get)
	router.POST("/user", user.Create)
	router.PUT("/user/:user_id", user.Update)
	router.DELETE("/user/:user_id", user.Delete)
	router.POST("/user/login", user.Login)

	// Internal endpoint
	router.GET("/internal/user/search", user.Search)

}
