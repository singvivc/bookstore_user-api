package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/singvivc/bookstore_utils-go/logger"
)

var router = gin.Default()

func StartBookStoreUserApplication() {
	mapUrls()
	logger.Info("About to start the book store User api application...")
	router.Run(":8081")
}
