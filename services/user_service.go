/**
	Package services contains the function to find, create and search for the user.
**/
package services

import (
	"gitlab.com/singvivc/bookstore_user-api/domains/user"
	"gitlab.com/singvivc/bookstore_utils-go/date"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
)

var UserService userInterface = &userService{}

type userService struct{}

type userInterface interface {
	GetUser(int64) (*user.User, errors.RestError)
	CreateUser(user.User) (*user.User, errors.RestError)
	UpdateUser(user.User) (*user.User, errors.RestError)
	DeleteUser(int64) errors.RestError
	Search(string) (user.Users, errors.RestError)
	LoginUser(user.LoginRequest) (*user.User, errors.RestError)
}

// GetUser function that gets the user for the given userId
func (service *userService) GetUser(userId int64) (*user.User, errors.RestError) {
	result := &user.User{Id: userId}
	if err := result.Get(); err != nil {
		return nil, err
	}
	return result, nil
}

// CreateUser function creates a new user provided all validation is successful
func (service *userService) CreateUser(newUser user.User) (*user.User, errors.RestError) {
	if err := newUser.Validate(); err != nil {
		return nil, err
	}
	newUser.Status = user.Active
	newUser.CreatedOn = date.GetDBFormatTime()
	newUser.UpdatedOn = date.GetDBFormatTime()
	if err := newUser.Save(); err != nil {
		return nil, err
	}
	return &newUser, nil
}

func (service *userService) UpdateUser(user user.User) (*user.User, errors.RestError) {
	currentUser, err := service.GetUser(user.Id)
	if err != nil {
		return nil, err
	}
	currentUser.From(user)
	if err := currentUser.Update(); err != nil {
		return nil, err
	}
	return currentUser, nil
}

func (service *userService) DeleteUser(userId int64) errors.RestError {
	user := &user.User{Id: userId}
	return user.Delete()
}

func (service *userService) Search(status string) (user.Users, errors.RestError) {
	user := &user.User{}
	return user.FindByStatus(status)
}

func (service *userService) LoginUser(request user.LoginRequest) (*user.User, errors.RestError) {
	user := &user.User{Email: request.Email}
	if err := user.FindByEmailAndPassword(request.Password); err != nil {
		return nil, err
	}
	return user, nil
}
