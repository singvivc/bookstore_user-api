module gitlab.com/singvivc/bookstore_user-api.git

go 1.14

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/go-sql-driver/mysql v1.5.0
	gitlab.com/singvivc/bookstore_oauth-go v0.0.0-20200420192607-8306d45b21f4
	gitlab.com/singvivc/bookstore_user-api v0.0.0-20200420185434-55d6415b87dd
	gitlab.com/singvivc/bookstore_utils-go v0.0.0-20200420191921-591738e9b2e5
	golang.org/x/crypto v0.0.0-20200420104511-884d27f42877
)
